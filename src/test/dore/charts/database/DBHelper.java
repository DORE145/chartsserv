package test.dore.charts.database;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.dore.charts.MainServlet;
import test.dore.charts.sources.sdfmon.DataItem;
import test.dore.charts.sources.sdfmon.RecordItem;

public final class DBHelper {

	private static final String RECIVE_ID_COMMAND = "select \"monitoring\".\"monitoring::ids\".nextval from DUMMY;";
	private static DataSource dataSource;
	private static Logger logger;
	private static ExecutorService executor;
	
	static {
		logger = LoggerFactory.getLogger(DBHelper.class);
		initializeDataSource();
		executor = MainServlet.getExecutor();
	}

	public static int getId() throws SQLException {
		Connection connection = null;
		Statement  statement = null;
		ResultSet resultSet = null;
		if (dataSource == null) initializeDataSource();
		if (dataSource != null) {
			try {
				connection = dataSource.getConnection();
				statement = connection.createStatement();
				resultSet = statement.executeQuery(RECIVE_ID_COMMAND);
				resultSet.next();
				return resultSet.getInt(1);
			} catch (SQLException e) {
				logger.error("Can't enstablish connection to database");
				throw e;
			} finally {
				try {
					if (resultSet != null) {
						resultSet.close();
					}
					if (statement != null) {
						statement.close();
					}
					if (connection != null) {
						connection.close();
					} 
				} catch (SQLException e) {
					logger.error("Connection closing error: " + e.getMessage());
				}
			}
		} else {
			logger.error("Database not found");
		}
		return -1;		
	}
	
	public static void sendRecord(RecordItem item, HttpServletResponse response) throws IOException {
		if (dataSource == null) initializeDataSource();
		if (dataSource == null) return;
		try {
			new RecordsSender(item, dataSource).send();
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			PrintWriter writer = response.getWriter();
			writer.println("Record Creating Error");
			writer.flush();
			writer.close();
		}
	}
	
	public static void sendData(DataItem item, int counter) {
		if (dataSource == null) initializeDataSource();
		if (dataSource == null) return;
		executor.execute(new DataSender(item, dataSource, counter));
	}
	
	public static void deleteData(String idString, HttpServletResponse response) throws IOException {
		if (dataSource == null) initializeDataSource();
		if (dataSource == null) return;
		String ids[] = idString.split(",");
		for (int i = 0; i < ids.length; i++) {
			int id = Integer.parseInt(ids[i]);
			executor.execute(new DataDeleter(id, dataSource));
			try {
				new RecordsDeleter(id, dataSource).delete();
			} catch (SQLException e) {
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				PrintWriter writer = response.getWriter();
				writer.println("Record deletion error");
				writer.flush();
				writer.close();
				return;
			}
		}
	}
	
	private static void initializeDataSource() {
		try {
			InitialContext ctx = new InitialContext();
			dataSource = (DataSource)ctx.lookup("java:comp/env/jdbc/stats");
			logger.info("Database found");
		} catch (NamingException e) {
			logger.error("Can't find database");
		}
	}
	
	public static boolean checkConnection() {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement("SELECT count(*) FROM DUMMY");
			statement.executeQuery();
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				logger.error("Connection closing error: " + e.getMessage());
			}
		}
	}
	
	
	
}
