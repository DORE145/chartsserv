package test.dore.charts.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RecordsDeleter {

	private static final String RECORDS_TABLE = "\"monitoring\".\"monitoring::records\"";
	private int ID;
	private DataSource dataSource;
	private Logger logger;
	
	public RecordsDeleter(int ID, DataSource dataSource) {
		this.ID = ID;
		this.dataSource = dataSource;
		logger = LoggerFactory.getLogger(DataDeleter.class);
	}
	
	public void delete() throws SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			logger.debug("Deleteing record with ID: " + ID);
			connection = dataSource.getConnection();
			String sqlStatement = "DELETE FROM " + RECORDS_TABLE
					+ "WHERE \"Id\" = ?;"; 
			statement = connection.prepareStatement(sqlStatement);
			statement.setInt(1, ID);
			statement.execute();
			logger.debug("Deleted data with id: " + ID + " at: "+ new Date().toString());
		} catch (SQLException e) {
			logger.error("SQL exception in data deleter: " + e.getMessage());
			throw e;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				logger.error("Closing connection error: " + e.getMessage());
			}
		}
	}

}
