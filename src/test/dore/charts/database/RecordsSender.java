package test.dore.charts.database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.dore.charts.sources.sdfmon.RecordItem;

public class RecordsSender {

	private static final String RECORDS_TABLE = "\"monitoring\".\"monitoring::records\"";
	private RecordItem item;
	private DataSource dataSource;
	private Logger logger;
	
	public RecordsSender(RecordItem item, DataSource dataSource) {
		this.item = item;
		this.dataSource = dataSource;
		logger = LoggerFactory.getLogger(RecordsSender.class);
	}
	
	public void send() throws SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = dataSource.getConnection();
			String sqlStatement = "INSERT INTO " + RECORDS_TABLE 
					+ " (\"Id\", \"CustomerName\", \"Source\", \"ServiceType\", \"SID\", \"Date\", \"Aggregation\", \"TimeFrom\", \"TimeTo\") "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
			statement = connection.prepareStatement(sqlStatement);
			
			statement.setInt(1, item.getId());
			statement.setString(2, item.getCustomerName());
			statement.setString(3, item.getSource());
			statement.setString(4, item.getServiceType());
			statement.setString(5, item.getSID());
			Date date = new Date(item.getDate().getTime());
			statement.setDate(6, date);
			statement.setInt(7, item.getAggregation());
			Time timeFrom = new Time(item.getTimeFrom().getTime());
			statement.setTime(8, timeFrom);
			Time timeTo = new Time(item.getTimeTo().getTime());
			statement.setTime(9, timeTo);
			statement.execute();
			
			logger.debug("RecordIten sended at: " + new java.util.Date().toString());
		} catch (SQLException e) {
			logger.error("SQL exception in data sender: " + e.getMessage());
			throw e;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				logger.error("Connection closing error: " + e.getMessage());
			}
		}

	}

}
