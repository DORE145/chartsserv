package test.dore.charts.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.dore.charts.sources.sdfmon.DataItem;

public class DataSender implements Runnable {

	private static final String DATA_TABLE = "\"monitoring\".\"monitoring::data\"";
	
	private DataItem item;
	private DataSource dataSource;
	private Logger logger;
	private int counter;
	
	public DataSender(DataItem item, DataSource dataSource, int counter) {
		this.item = item;
		this.dataSource = dataSource;
		this.counter = counter;
		logger = LoggerFactory.getLogger(DataSender.class);
	}
	
	@Override
	public void run() {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			logger.debug("Starting DataSender for line: " + counter);
			connection = dataSource.getConnection();
			String sqlStatement = "INSERT INTO " + DATA_TABLE
					+ " (\"Id\", \"AS_Instance\", \"ACT_WPS\", \"DIA_WPS\", \"RFC_WPS\", \"CPU_USR\", \"CPU_SYS\", \"CPU_ACT\", \"CPU_IDLE\", \"AVA_CPU\", "
					+ "\"PAGING_IN\", \"PAGING_OUT\", \"FREE_MEM\", \"EM_ALLOC\", \"EM_ATTACH\", \"EM_GLOBAL\", \"HEAP\", \"PRIV\", \"PAGE_SHM\", \"ROLL_SHM\", "
					+ "\"DIA_QUEUE\", \"UPD_QUEUE\", \"ENQ_QUEUE\", \"LOGINS\", \"SESSIONS\", \"ENQ_ENTRIES\", \"IN_QUEUE\", \"OUT_QUEUE\", \"Time\") "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			statement = connection.prepareStatement(sqlStatement);
			statement.setInt(1, item.getId());
			statement.setString(2, item.getAS_Instance());
			statement.setInt(3, item.getAct_WPs());
			statement.setInt(4, item.getDia_WPs());
			statement.setInt(5, item.getRFC_WPs());
			statement.setInt(6, item.getCPU_Usr());
			statement.setInt(7, item.getCPU_Sys());
			statement.setInt(8, item.getCPU_Act());
			statement.setInt(9, item.getCPU_Idle());
			statement.setInt(10, item.getAva_CPU());
			statement.setInt(11, item.getPaging_In());
			statement.setInt(12, item.getPaging_Out());
			statement.setInt(13, item.getFree_Mem());
			statement.setInt(14, item.getEM_Alloc());
			statement.setInt(15, item.getEM_Attach());
			statement.setInt(16, item.getEM_Global());
			statement.setInt(17, item.getHeap());
			statement.setInt(18, item.getPriv());
			statement.setInt(19, item.getPage_SHM());
			statement.setInt(20, item.getRoll_SHM());
			statement.setInt(21, item.getDia_Queue());
			statement.setInt(22, item.getUpd_Queue());
			statement.setInt(23, item.getEnq_Queue());
			statement.setInt(24, item.getLogins());
			statement.setInt(25, item.getSessions());
			statement.setInt(26, item.getEnq_Entries());
			statement.setInt(27, item.getIn_Queue());
			statement.setInt(28, item.getOut_Queue());
			Time time = new Time(item.getTime().getTime());
			statement.setTime(29, time);
			
			statement.execute();
			logger.debug("DataItem number: " + counter + ", sended at: " + new java.util.Date().toString());
		} catch (SQLException e) {
			logger.error("SQL exception in data sender for item with \n "
					+ "id: " + item.getId() + " instance: " + item.getAS_Instance() + " time: " + item.getTime()  + " \n " + e.getMessage());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				logger.error("Connection closing error: " + e.getMessage());
			}
		}
		
	}

}