package test.dore.charts.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataDeleter implements Runnable {

	private static final String DATA_TABLE = "\"monitoring\".\"monitoring::data\"";
	
	private int ID;
	private DataSource dataSource;
	private Logger logger;
	
	public DataDeleter(int ID, DataSource dataSource) {
		this.ID = ID;
		this.dataSource = dataSource;
		logger = LoggerFactory.getLogger(DataDeleter.class);
	}
	
	@Override
	public void run() {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			logger.debug("Deleteing data with ID: " + ID);
			connection = dataSource.getConnection();
			String sqlStatement = "DELETE FROM " + DATA_TABLE
					+ "WHERE \"Id\" = ?;"; 
			statement = connection.prepareStatement(sqlStatement);
			statement.setInt(1, ID);
			statement.execute();
			logger.debug("Deleted data with id: " + ID + " at: "+ new Date().toString());
		} catch (SQLException e) {
			logger.error("SQL exception in data deleter: " + e.getMessage());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				logger.error("Connection closing error: " + e.getMessage());
			}
		}
	}
}
