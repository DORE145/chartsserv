package test.dore.charts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.dore.charts.database.DBHelper;
import test.dore.charts.sources.sdfmon.Processor;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet(asyncSupported = true, urlPatterns = { "/" })
@MultipartConfig
public class MainServlet extends HttpServlet {
	
	private static final long serialVersionUID = -5453244239614942467L;
	private static ExecutorService executor;
	private static Logger logger;
	
	@Override
	public void init() throws ServletException {
		if (executor == null) {
			executor = Executors.newCachedThreadPool();
		}
		if (logger == null) {
			logger = LoggerFactory.getLogger(MainServlet.class);
		}
		super.init();
	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		fixHeaders(response);
		super.service(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!DBHelper.checkConnection()) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			PrintWriter writer = response.getWriter();
			writer.println("No database connection");
			writer.flush();
			writer.close();
			return;
		}
		String type = request.getParameter("type");
		if (type.equals("upload")) {
			logger.debug("File recieved at:" + new Date().getTime());
			String source = request.getParameter("source");
			if (source.equals("/SDF/MON")) {
				new Processor().process(request, response);
			}
		} else if (type.equals("delete")) {
			String idString = request.getParameter("id");
			DBHelper.deleteData(idString, response);
		} else {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			PrintWriter writer = response.getWriter();
			writer.println("Bad request.");
			writer.flush();
			writer.close();
		}
	}
	
	private void fixHeaders(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Max-Age", "86400");
	}

	public static ExecutorService getExecutor() {
		return executor;
	}
}
