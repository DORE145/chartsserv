package test.dore.charts.sources.sdfmon;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.dore.charts.database.DBHelper;

public class Parser implements Runnable {

	private Path file;
	private HashMap<Date, RecordItem> dateRecord;
	private HashMap<String, String> namesMap;
	private Logger logger;
	private Date defaultDate;
	
	public Parser(Path file, HashMap<Date, RecordItem> dateRecord, HashMap<String, String> namesMap, Date defaultDate) {
		this.file = file;
		this.dateRecord = dateRecord;
		this.namesMap = namesMap;
		this.defaultDate = defaultDate;
		logger = LoggerFactory.getLogger(Parser.class);
	} 
	
	@Override
	public void run() {
		int counter = 0;
		try {
			//Reading file header
			BufferedReader fileReader = Files.newBufferedReader(file);
			String firstLine = fileReader.readLine();
			if (!firstLine.contains("-")) {
				fileReader.readLine();
			}
			String columnsLine = fileReader.readLine();
			String[] columnsNames = columnsLine.split("\\|");
			
			//Deciding which column contains which value 
			String[] columnValueName = new String[columnsNames.length];
			for (int i = 0; i < columnsNames.length; i++) {
				columnsNames[i] = columnsNames[i].trim();
				if (namesMap.containsKey(columnsNames[i])) {
					columnValueName[i] = namesMap.get(columnsNames[i]);
					if (i > 0 && columnsNames[i-1] != null && columnsNames[i-1].equals(columnsNames[i])) {
						columnValueName[i] = namesMap.get(columnsNames[i] + "2"); 
					}
				}
			}
			
			//Deciding decimal point
			fileReader.readLine();
			String dataLine = fileReader.readLine();
			String DECIMAL_SEPARATOR = "\\.";
			String THOUSANDS_SEPARATOR = ",";
			String[] dataValues = dataLine.split("\\|");
			for (int i = 0; i < columnValueName.length; i++) {
				if (columnValueName[i] == null) continue;
				if (columnValueName[i].equals("Free_Mem") || columnValueName[i].equals("EM_Global")) {
					if (dataValues[i].contains(".")) {
						DECIMAL_SEPARATOR = ",";
						THOUSANDS_SEPARATOR = "\\.";
					} else {
						DECIMAL_SEPARATOR = "\\.";
						THOUSANDS_SEPARATOR = ",";
					}
				}
			}
			while (fileReader.ready() || !dataLine.contains("---")) {
				dataValues = dataLine.split("\\|");
				dataLine = fileReader.readLine(); // Reading next line before checking previous line to be able to skip loop cycle 
				
				// If length of this two arrays not math then there is incorrect line in the file or file corrupted
				if (dataValues.length != columnValueName.length) continue;
				DataItem item = new DataItem();
				for (int i = 0; i < dataValues.length; i++) {
					try {
						if (columnValueName[i] == null) continue; 
						if (!columnValueName[i].equals("Date") && !columnValueName[i].equals("Time")) {
							dataValues[i] = dataValues[i].replaceAll(THOUSANDS_SEPARATOR, "");							
						}
						String value = dataValues[i].trim();
						
						switch (columnValueName[i]) {
							case "Date": Date date = new SimpleDateFormat("dd.MM.yy").parse(value); 
										 RecordItem recordItem = dateRecord.get(date);
										 if (recordItem != null) {
											 int id = recordItem.getId();
											 item.setId(id);
										 }
										 break;
							case "Time": Date time = new SimpleDateFormat("HH:mm:ss").parse(value);
										 item.setTime(time);
										 break;
							case "AS_Instance": item.setAS_Instance(value);
												break;
							case "Act_WPs": item.setAct_WPs(Integer.parseInt(value)); break;
							case "Dia_WPs": item.setDia_WPs(Integer.parseInt(value)); break;
							case "RFC_WPs": item.setRFC_WPs(Integer.parseInt(value)); break;
							case "CPU_Usr": item.setCPU_Usr(Integer.parseInt(value)); break;
							case "CPU_Sys": item.setCPU_Sys(Integer.parseInt(value)); break;
							case "CPU_Idle": item.setCPU_Idle(Integer.parseInt(value)); break;
							case "Ava_CPU": item.setAva_CPU(Integer.parseInt(value)); break;
							case "Paging_In": item.setPaging_In(Integer.parseInt(value)); break;
							case "Paging_Out": item.setPaging_Out(Integer.parseInt(value)); break;
							case "Free_Mem": item.setFree_Mem(Integer.parseInt(value)); break;
							case "EM_Alloc": item.setEM_Alloc(Integer.parseInt(value)); break;
							case "EM_Attach": item.setEM_Attach(Integer.parseInt(value)); break;
							case "EM_Global": item.setEM_Global(Integer.parseInt(value)); break;
							case "Heap": item.setHeap(Integer.parseInt(value)); break;
							case "Priv": item.setPriv(Integer.parseInt(value)); break;
							case "Page_SHM": item.setPage_SHM(Integer.parseInt(value)); break;
							case "Roll_SHM": item.setRoll_SHM(Integer.parseInt(value)); break;
							case "Dia_Queue": item.setDia_Queue(Integer.parseInt(value)); break;
							case "Upd_Queue": item.setUpd_Queue(Integer.parseInt(value)); break;
							case "Enq_Queue": item.setUpd_Queue(Integer.parseInt(value)); break;
							case "Logins": item.setLogins(Integer.parseInt(value)); break;
							case "Sessions": item.setSessions(Integer.parseInt(value)); break;
							case "Enq_Entries": item.setEnq_Entries(Integer.parseInt(value)); break;
							case "In_Queue": item.setIn_Queue(Integer.parseInt(value)); break;
							case "Out_Queue": item.setOut_Queue(Integer.parseInt(value)); break;
						}
					} catch (NumberFormatException e) {
						logger.error("Data value parse error " + e.getMessage() + " for field: " + columnValueName[i]);
						continue;
					}
				}

				//In case that there no Date column in the file
				if (item.getId() == 0) {
					RecordItem defaultItem = dateRecord.get(defaultDate); 
					int defaultId = defaultItem.getId();
					item.setId(defaultId);
				}
				
				DBHelper.sendData(item, ++counter);
			}
			
			fileReader.close();
			Files.delete(file);
		} catch (IOException e) {
			logger.error("Can't read recived file: " + e.getMessage());
		} catch (ParseException e) {
			logger.error("Time parsing error: " + e.getMessage());
		}

	}

}
