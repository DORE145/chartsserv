package test.dore.charts.sources.sdfmon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import test.dore.charts.MainServlet;
import test.dore.charts.database.DBHelper;

public class Processor {

	private final String SOURCE = "/SDF/MON";
	private Logger logger;
	private HashMap<String, String> namesMap;
	
	public Processor() {
		logger = LoggerFactory.getLogger(Processor.class);
		namesMap = new HashMap<String, String>();
		namesMap.put("Time", "Time");
		namesMap.put("Date", "Date");
		namesMap.put("AS_Instance", "AS_Instance");
		namesMap.put("AS Instance", "AS_Instance");
		namesMap.put("Server_Name", "AS_Instance");
		namesMap.put("Server Name", "AS_Instance");
		namesMap.put("Act_WPs", "Act_WPs");
		namesMap.put("Act. WPs", "Act_WPs");
		namesMap.put("Act. WPs", "Act_WPs");
		namesMap.put("Dia_WPs", "Dia_WPs");
		namesMap.put("Dia.WPs", "Dia_WPs");
		namesMap.put("RFC WPs", "RFC_WPs");
		namesMap.put("RFC_WPs", "RFC_WPs");
		namesMap.put("CPU Usr", "CPU_Usr");
		namesMap.put("CPU_Usr", "CPU_Usr");
		namesMap.put("CPU Sys", "CPU_Sys");
		namesMap.put("CPU_Sys", "CPU_Sys");
		namesMap.put("CPU Idle", "CPU_Idle");
		namesMap.put("CPU_Idle", "CPU_Idle");
		namesMap.put("Ava.", "Ava_CPU");
		namesMap.put("Ava_CPU", "Ava_CPU");
		namesMap.put("Paging in", "Paging_In");
		namesMap.put("Paging_In", "Paging_In");
		namesMap.put("Paging out", "Paging_Out");
		namesMap.put("Paging_Out", "Paging_Out");
		namesMap.put("Free Mem.", "Free_Mem");
		namesMap.put("Free_Mem", "Free_Mem");
		namesMap.put("EM alloc.", "EM_Alloc");
		namesMap.put("EM_Alloc", "EM_Alloc");
		namesMap.put("EM attach.", "EM_Attach");
		namesMap.put("EM_Attach", "EM_Attach");
		namesMap.put("Em global", "EM_Global");
		namesMap.put("EM_Global", "EM_Global");
		namesMap.put("Hea.", "Heap");
		namesMap.put("Heap", "Heap");
		namesMap.put("Heap Memor", "Heap");
		namesMap.put("Pri.", "Priv");
		namesMap.put("Priv", "Priv");
		namesMap.put("Paging Mem", "Page_SHM");
		namesMap.put("Page SHM", "Page_SHM");
		namesMap.put("Page_SHM", "Page_SHM");
		namesMap.put("Roll Mem", "Roll_SHM");
		namesMap.put("Roll SHM", "Roll_SHM");
		namesMap.put("Roll_SHM", "Roll_SHM");
		namesMap.put("Dia.", "Dia_Queue");
		namesMap.put("Dia_Queue", "Dia_Queue");
		namesMap.put("Upd.", "Upd_Queue");
		namesMap.put("Upd_Queue", "Upd_Queue");
		namesMap.put("Enq.", "Enq_Queue");
		namesMap.put("Enq_Queue.", "Enq_Queue");
		namesMap.put("Logins", "Logins");
		namesMap.put("Sessions", "Sessions");
		namesMap.put("Num.", "Enq_Entries");
		namesMap.put("Enq_Entries", "Enq_Entries");
		namesMap.put("Queue", "In_Queue");
		namesMap.put("In_Queue", "In_Queue");
		namesMap.put("Queue2", "Out_Queue");
		namesMap.put("Out_Queue", "Out_Queue");
	}
	
	public void process(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			//Retrieving request information
			long startTime = new Date().getTime();
			logger.debug("Processing started at: " + startTime);
			String dateString = request.getParameter("date");
			Date date = new SimpleDateFormat("dd.MM.yy").parse(dateString);
			String customerName = request.getParameter("customer");
			String serviceType = request.getParameter("serviceType");
			String sid = request.getParameter("sid");
			Part filePart = request.getPart("file");
			
			//Create temp file from received file to be able to use it in parser thread 
			Path file = Files.createTempFile("tempFile", null);
			BufferedWriter fileWrter = Files.newBufferedWriter(file, StandardOpenOption.WRITE);
			BufferedReader fileReader = new BufferedReader(new InputStreamReader(filePart.getInputStream()));
			while (fileReader.ready()) {
				fileWrter.write(fileReader.readLine() + "\n");
			}
			fileReader.close();
			fileWrter.close();
			
			logger.debug("File copied at: " + new Date().getTime());
			//Calculating records attributes
			HashMap<Date, RecordItem> dateRecord = new HashMap<>();
			Set<String> intancesNamesSet = new HashSet<>();
			int dateColumnId = -1;
			int timeColumnId = -1;
			int instancesColumnId = -1;
			
			//Searching for columns with for format specifications in mind
			fileReader = Files.newBufferedReader(file);
			String firstLine = fileReader.readLine();
			if (!firstLine.contains("-")) {
				fileReader.readLine();
			}
			String columnsLine = fileReader.readLine();
			String[] columnsNames = columnsLine.split("\\|");
			for (int i = 0; i < columnsNames.length; i++) {
				String name = columnsNames[i].trim();
				if (name.equals("Time")) timeColumnId = i;
				if (name.equals("Date")) dateColumnId = i;
				if (name.equals("AS_Instance") || name.equals("AS Instance") 
						|| name.equals("Server_Name") || name.equals("Server Name")) {
					instancesColumnId = i;
				}
			}
			
			//Reading, retrieving and parsing values
			fileReader.readLine();
			String dataLine = fileReader.readLine();
			while (fileReader.ready() || !dataLine.contains("---")) {
				String[] dataValues = dataLine.split("\\|");
				dataLine = fileReader.readLine();
				
				Date dataDate;  
				if (dateColumnId == -1) {
					dataDate = date;
				} else {
					dataDate = new SimpleDateFormat("dd.MM.yy").parse(dataValues[dateColumnId].trim());
				}
				
				Date time = new SimpleDateFormat("HH:mm:ss").parse(dataValues[timeColumnId].trim());
				
				RecordItem item;
				if (dateRecord.containsKey(dataDate)) {
					item = dateRecord.get(dataDate);
				} else {
					item = new RecordItem();
					item.setDate(dataDate);
					dateRecord.put(dataDate, item);
				}
				item.checkTime(time);
				item.addCount();
				
				if (instancesColumnId != -1) intancesNamesSet.add(dataValues[instancesColumnId].trim()); 
			}
			
			logger.debug("File analyzaed at: " + new Date().getTime());
			RecordItem biggestRecord = null;
			//Generating id and passing request attributes from request to records
			for (RecordItem item : dateRecord.values()) {
				item.setId(generateId());
				item.setCustomerName(customerName);
				item.setSource(SOURCE);
				item.setServiceType(serviceType);
				item.setSID(sid);
				
				long timePeriod = item.getTimeTo().getTime() - item.getTimeFrom().getTime();
				int numberOfInstances = intancesNamesSet.size() == 0 ? 1 : intancesNamesSet.size();
				long recordsPerMinute = item.getCount() / numberOfInstances / (timePeriod / 1000 / 60);
				
				//Aggregation time: one minute for every 3 hours of time period, one minute for every 5 instances and minus 1 minute if 2 or less records per minute
				int aggregatiomTime = (int)((timePeriod / 10_800_000) + (numberOfInstances / 4)); 
				if (recordsPerMinute <= 1) aggregatiomTime--;					
				if (aggregatiomTime < 1) aggregatiomTime = 0;
				item.setAggregation(aggregatiomTime);
				
				if (biggestRecord == null || item.getCount() > biggestRecord.getCount()) {
					biggestRecord = item;
				}
				
				//Sending Record
				sendRecord(item, response); 
			}
			
			fileReader.close();
			
			long endTime = new Date().getTime();
			logger.debug("Records created at: " + endTime);
			logger.debug("File processing took: " + (endTime - startTime) + "ms");
			MainServlet.getExecutor().execute(new Parser(file, dateRecord, namesMap, date));
			
			response.setStatus(HttpServletResponse.SC_CREATED);
			PrintWriter writer = response.getWriter();
			writer.println(biggestRecord.getId());
			writer.println(biggestRecord.getCount());
			writer.flush();
			writer.close();
			
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			PrintWriter writer = response.getWriter();
			writer.println("Value format error");
			writer.flush();
			writer.close();
			return;
		} catch (IOException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			PrintWriter writer = response.getWriter();
			writer.println("File Format Error");
			writer.flush();
			writer.close();
			return;
		} catch (ParseException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			PrintWriter writer = response.getWriter();
			writer.println("Time Format Error");
			writer.flush();
			writer.close();
			return;
		} catch (Exception e) {
			logger.error("Reqest processing error: " + e.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			PrintWriter writer = response.getWriter();
			writer.println("Processing error.");
			writer.flush();
			writer.close();
			return;
		}
	}
	
	private final int generateId() throws SQLException {
		return DBHelper.getId();
	}
	
	private final void sendRecord(RecordItem item, HttpServletResponse response) throws IOException {
		DBHelper.sendRecord(item, response);
	}
}
