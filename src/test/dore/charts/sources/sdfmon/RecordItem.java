package test.dore.charts.sources.sdfmon;

import java.util.Date;

public class RecordItem {
	
	private int Id;
	private String CustomerName;
	private String Source;
	private String ServiceType;
	private String SID;
	private Date Date;
	private int Aggregation;
	private Date TimeFrom;
	private Date TimeTo;
	private int count;
	
	public int getId() {
		return Id;
	}
	
	public void setId(int id) {
		Id = id;
	}
	
	public String getCustomerName() {
		return CustomerName;
	}
	
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	
	public String getSource() {
		return Source;
	}
	
	public void setSource(String source) {
		Source = source;
	}
	
	public String getServiceType() {
		return ServiceType;
	}
	
	public void setServiceType(String serviceType) {
		ServiceType = serviceType;
	}
	
	public String getSID() {
		return SID;
	}
	
	public void setSID(String sID) {
		SID = sID;
	}
	
	public Date getDate() {
		return Date;
	}
	
	public void setDate(Date date) {
		Date = date;
	}

	public int getAggregation() {
		return Aggregation;
	}

	public void setAggregation(int aggregation) {
		Aggregation = aggregation;
	}

	public Date getTimeFrom() {
		return TimeFrom;
	}

	public void setTimeFrom(Date timeFrom) {
		TimeFrom = timeFrom;
	}

	public Date getTimeTo() {
		return TimeTo;
	}

	public void setTimeTo(Date timeTo) {
		TimeTo = timeTo;
	}

	public void checkTime(Date time) {
		if (this.TimeFrom == null || this.TimeFrom.getTime() - time.getTime() > 0) {
			this.TimeFrom = time;
			}
		if (this.TimeTo == null || this.TimeTo.getTime() - time.getTime() < 0) {
			this.TimeTo = time;
			}
	}
	
	public void addCount() {
		this.count++;
	}
	
	public int getCount() {
		return this.count;
	}
}
