package test.dore.charts.sources.sdfmon;

import java.util.Date;

public class DataItem {

	private int Id;
	private String AS_Instance;
	private int Act_WPs;
	private int Dia_WPs;
	private int RFC_WPs;
	private int CPU_Usr;
	private int CPU_Sys;
	private int CPU_Act;
	private int CPU_Idle;
	private int Ava_CPU;
	private int Paging_In;
	private int Paging_Out;
	private int Free_Mem;
	private int EM_Alloc;
	private int EM_Attach;
	private int EM_Global;
	private int Heap;
	private int Priv;
	private int Page_SHM;
	private int Roll_SHM;
	private int Dia_Queue;
	private int Upd_Queue;
	private int Enq_Queue;
	private int Logins;
	private int Sessions;
	private int Enq_Entries;
	private int In_Queue;
	private int Out_Queue;
	private Date Time;
	
	public int getId() {
		return Id;
	}
	
	public void setId(int Id) {
		this.Id = Id;
	}
	
	public String getAS_Instance() {
		return AS_Instance;
	}
	
	public void setAS_Instance(String aS_Instance) {
		AS_Instance = aS_Instance;
	}
	
	public int getAct_WPs() {
		return Act_WPs;
	}
	
	public void setAct_WPs(int act_WPs) {
		Act_WPs = act_WPs;
	}
	
	public int getDia_WPs() {
		return Dia_WPs;
	}
	
	public void setDia_WPs(int dia_WPs) {
		Dia_WPs = dia_WPs;
	}
	
	public int getRFC_WPs() {
		return RFC_WPs;
	}
	
	public void setRFC_WPs(int rFC_WPs) {
		RFC_WPs = rFC_WPs;
	}
	
	public int getCPU_Usr() {
		return CPU_Usr;
	}
	
	public void setCPU_Usr(int cPU_Usr) {
		CPU_Usr = cPU_Usr;
		setCPU_Act(CPU_Usr + CPU_Sys); 
	}
	
	public int getCPU_Sys() {
		return CPU_Sys;
	}
	
	public void setCPU_Sys(int cPU_Sys) {
		CPU_Sys = cPU_Sys;
		setCPU_Act(CPU_Usr + CPU_Sys);
	}
	
	public int getCPU_Act() {
		return CPU_Act;
	}
	
	private void setCPU_Act(int cPU_Act) {
		CPU_Act = cPU_Act;
	}
	
	public int getCPU_Idle() {
		return CPU_Idle;
	}
	
	public void setCPU_Idle(int cPU_Idle) {
		CPU_Idle = cPU_Idle;
	}
	
	public int getAva_CPU() {
		return Ava_CPU;
	}
	
	public void setAva_CPU(int ava_CPU) {
		Ava_CPU = ava_CPU;
	}
	
	public int getPaging_In() {
		return Paging_In;
	}
	
	public void setPaging_In(int paging_In) {
		Paging_In = paging_In;
	}
	
	public int getPaging_Out() {
		return Paging_Out;
	}
	
	public void setPaging_Out(int paging_Out) {
		Paging_Out = paging_Out;
	}
	
	public int getFree_Mem() {
		return Free_Mem;
	}
	
	public void setFree_Mem(int free_Mem) {
		Free_Mem = free_Mem;
	}
	
	public int getEM_Alloc() {
		return EM_Alloc;
	}
	
	public void setEM_Alloc(int eM_Alloc) {
		EM_Alloc = eM_Alloc;
	}
	
	public int getEM_Attach() {
		return EM_Attach;
	}
	
	public void setEM_Attach(int eM_Attach) {
		EM_Attach = eM_Attach;
	}
	
	public int getEM_Global() {
		return EM_Global;
	}
	
	public void setEM_Global(int eM_Global) {
		EM_Global = eM_Global;
	}
	
	public int getHeap() {
		return Heap;
	}
	
	public void setHeap(int heap) {
		Heap = heap;
	}
	
	public int getPriv() {
		return Priv;
	}
	
	public void setPriv(int priv) {
		Priv = priv;
	}
	
	public int getPage_SHM() {
		return Page_SHM;
	}
	
	public void setPage_SHM(int page_SHM) {
		Page_SHM = page_SHM;
	}
	
	public int getRoll_SHM() {
		return Roll_SHM;
	}
	
	public void setRoll_SHM(int roll_SHM) {
		Roll_SHM = roll_SHM;
	}
	
	public int getDia_Queue() {
		return Dia_Queue;
	}
	
	public void setDia_Queue(int dia_Queue) {
		Dia_Queue = dia_Queue;
	}
	
	public int getUpd_Queue() {
		return Upd_Queue;
	}
	
	public void setUpd_Queue(int upd_Queue) {
		Upd_Queue = upd_Queue;
	}
	
	public int getEnq_Queue() {
		return Enq_Queue;
	}
	
	public void setEnq_Queue(int enq_Queue) {
		Enq_Queue = enq_Queue;
	}
	
	public int getLogins() {
		return Logins;
	}
	
	public void setLogins(int logins) {
		Logins = logins;
	}
	
	public int getSessions() {
		return Sessions;
	}
	
	public void setSessions(int sessions) {
		Sessions = sessions;
	}
	
	public int getEnq_Entries() {
		return Enq_Entries;
	}
	
	public void setEnq_Entries(int enq_Entries) {
		Enq_Entries = enq_Entries;
	}
	
	public int getIn_Queue() {
		return In_Queue;
	}
	
	public void setIn_Queue(int in_Queue) {
		In_Queue = in_Queue;
	}
	
	public int getOut_Queue() {
		return Out_Queue;
	}
	
	public void setOut_Queue(int out_Queue) {
		Out_Queue = out_Queue;
	}
	
	public Date getTime() {
		return Time;
	}
	
	public void setTime(Date time) {
		Time = time;
	}
	
}
